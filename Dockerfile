FROM nginx
RUN mkdir -p /var/www/web
COPY ./build /var/www/web
COPY nginx.conf /etc/nginx/conf.d/default.conf