const images = new Map([
  ['руки', '/static/images/products/hands.svg'],
  ['ноги', '/static/images/products/legs.svg'],
  ['лицо', '/static/images/products/face.svg'],
  ['бикини', '/static/images/products/bikini.svg'],
  ['прочее', '/static/images/products/other.svg'],
]);

export const getImage = (value) => {
  return images.get(value.toLowerCase());
};
