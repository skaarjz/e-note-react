const set = (key, value) => {
  localStorage.setItem(key, value);
};

const get = (key) => {
  return localStorage.getItem(key);
};

const remove = (key) => {
  localStorage.removeItem(key);
};

export default {
  set,
  get,
  remove
};
