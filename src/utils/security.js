import axios from 'axios';
import localStorage from './localStorage';

const initAxios = () => {
  axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;
  // Добавляем Токен в header
  axios.interceptors.request.use((request) => {
    request.headers.common.authorization = `Bearer ${localStorage.get('access-token')}`;
    return request;
  }, undefined);
  axios.interceptors.response.use(undefined, async (error) => {
    // Удаляем токен, если приходит 401
    if (error?.response?.status === 401) {
      localStorage.remove('access-token');
    }
    throw error;
  });
};

export default {
  initAxios
};
