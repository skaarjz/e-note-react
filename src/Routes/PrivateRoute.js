import React from 'react';
import { Route, Navigate, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

const PrivateRoute = ({
  component: Component, path, isAuth
}) => {
  const { pathname } = useLocation();
  if (!isAuth) {
    return (
      <Navigate
        to="/login"
        replace
        state={{ from: pathname }}
      />
    );
  }
  return <Route path={path} element={<Component />} />;
};

PrivateRoute.propTypes = {
  component: PropTypes.object,
  path: PropTypes.string,
  isAuth: PropTypes.bool
};

export default PrivateRoute;
