import React, { useEffect } from 'react';
import {
  Navigate, Route, Routes,
} from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import LoginView from 'src/views/auth/LoginView';
import NotFoundView from 'src/views/errors/NotFoundView';
import { useDispatch } from 'react-redux';
import PrivateRoute from './PrivateRoute';
import { fetchUserInfo } from '../store/auth';
import WithCheckUserAuth from './WithCheckUserAuth';
import DelayedFallback from '../components/DelayedFallback';

const AccountView = React.lazy(() => import('src/views/account/AccountView'));
const CustomerListView = React.lazy(() => import('src/views/customer/CustomerListView'));
const DashboardView = React.lazy(() => import('src/views/reports/DashboardView'));
const ProductListView = React.lazy(() => import('src/views/product/ProductListView'));
const SettingsView = React.lazy(() => import('src/views/settings/SettingsView'));

const CustomRoutes = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchUserInfo());
  }, [dispatch]);
  return (
    <React.Suspense fallback={<DelayedFallback />}>
      <Routes>
        <Route path="/" element={<DashboardLayout />}>
          <Route path="/" element={<Navigate to="/dashboard" replace />} />
          <WithCheckUserAuth><PrivateRoute path="account" component={AccountView} /></WithCheckUserAuth>
          <WithCheckUserAuth><PrivateRoute path="customers" component={CustomerListView} /></WithCheckUserAuth>
          <WithCheckUserAuth><PrivateRoute path="dashboard" component={DashboardView} /></WithCheckUserAuth>
          <WithCheckUserAuth><PrivateRoute path="products" component={ProductListView} /></WithCheckUserAuth>
          <WithCheckUserAuth><PrivateRoute path="settings" component={SettingsView} /></WithCheckUserAuth>
        </Route>
        <Route path="/login" element={<WithCheckUserAuth><LoginView /></WithCheckUserAuth>} />
        <Route path="*" element={<NotFoundView />} />
      </Routes>
    </React.Suspense>
  );
};

export default CustomRoutes;
