import React, { useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import localStorage from '../utils/localStorage';
import { setToken } from '../store/auth';

const WithCheckUserAuth = (props) => {
  const dispatch = useDispatch();
  const token = localStorage.get('access-token');
  const isAuth = useSelector((state) => state.auth.auth.isAuth);
  const authBody = useMemo(() => ({ token, isAuth: !!token && isAuth }), [token, isAuth]);
  useEffect(() => {
    dispatch(setToken(authBody));
  }, [dispatch, authBody]);

  return React.cloneElement(props.children, { ...props, isAuth: !!token && isAuth });
};

export default WithCheckUserAuth;
