import axios from 'axios';

// Авторизация
const fetchToken = (authBody) => {
  const fetch = axios.create();
  return fetch.post('/auth/signIn', authBody, {
    baseURL: process.env.REACT_APP_BASE_URL
  });
};
// Получаем данные пользователя
const fetchUserInfo = () => axios.get('/auth/me');

// Получаем список категорий
const fetchCategories = () => axios.get('/api/categories/categories');

// Получаем список категория с кол-вом услуг
const fetchServices = () => axios.get('/api/services/count-services');

export default {
  fetchToken,
  fetchUserInfo,
  fetchCategories,
  fetchServices
};
