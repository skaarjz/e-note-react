import React, { useEffect } from 'react';
import {
  Box,
  Container,
  Grid,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'react-redux';
import Toolbar from './Toolbar';
import ProductCard from './ProductCard';
import { fetchCategoriesDetail } from '../../../store/products';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  productCard: {
    height: '100%'
  }
}));

const ProductList = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchCategoriesDetail());
  }, [dispatch]);
  const categories = useSelector((state) => state.products.categories);

  return (
    <Page
      className={classes.root}
      title="Products"
    >
      <Container maxWidth={false}>
        <Toolbar />
        <Box mt={3}>
          <Grid
            container
            spacing={3}
          >
            {categories.map((product) => (
              <Grid
                item
                key={product.id}
                lg={4}
                md={6}
                xs={12}
              >
                <ProductCard
                  className={classes.productCard}
                  product={product}
                />
              </Grid>
            ))}
          </Grid>
        </Box>
        {/* <Box */}
        {/*  mt={3} */}
        {/*  display="flex" */}
        {/*  justifyContent="center" */}
        {/* > */}
        {/*  <Pagination */}
        {/*    color="primary" */}
        {/*    count={3} */}
        {/*    size="small" */}
        {/*  /> */}
        {/* </Box> */}
      </Container>
    </Page>
  );
};

export default ProductList;
