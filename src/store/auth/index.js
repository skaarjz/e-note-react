import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import api from '../../api';
import localStorage from '../../utils/localStorage';

export const fetchToken = createAsyncThunk('auth/fetchToken',
  async (authBody) => {
    const { data: { accessToken } } = await api.fetchToken(authBody);
    localStorage.set('access-token', accessToken);
    return accessToken;
  });

export const fetchUserInfo = createAsyncThunk('auth/fetchUserInfo',
  async () => {
    const { data } = await api.fetchUserInfo();
    return data;
  });

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    auth: {
      token: '',
      isAuth: false,
    },
    user: {},
    error: null,
    status: 'loading'
  },
  reducers: {
    setToken: (state, action) => {
      const { token, isAuth } = action.payload;
      state.auth.token = token;
      state.auth.isAuth = isAuth;
    }
  },
  extraReducers: {
    [fetchToken.fulfilled]: (state, action) => {
      state.status = 'succeeded';
      state.auth.token = action.payload;
      // state.auth.isAuth = true;
    },
    [fetchToken.rejected]: (state, action) => {
      state.status = 'failed';
      state.error = action.error.message;
      state.auth.isAuth = false;
    },
    [fetchUserInfo.pending]: (state) => {
      state.status = 'loading';
    },
    [fetchUserInfo.fulfilled]: (state, action) => {
      state.status = 'succeeded';
      state.user = action.payload;
      state.auth.isAuth = true;
    },
    [fetchUserInfo.rejected]: (state, action) => {
      state.status = 'failed';
      state.error = action.error.message;
      state.auth.isAuth = false;
    }
  }
});
export const { setToken } = authSlice.actions;

export default authSlice.reducer;
