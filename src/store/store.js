import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import reducer from './rootReducer';

const middleware = [...getDefaultMiddleware()];

export default configureStore({ reducer, middleware });
