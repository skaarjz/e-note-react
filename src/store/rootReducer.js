import authReducer from './auth';
import productsSlice from './products';

const rootReducer = {
  auth: authReducer,
  products: productsSlice,
};

export default rootReducer;
