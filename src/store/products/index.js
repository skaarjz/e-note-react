import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import api from '../../api';
import { getImage } from '../../utils/getImage';

export const fetchCategories = createAsyncThunk('products/fetchCategories',
  async () => {
    const {
      data
    } = await api.fetchCategories();
    return data.reduce((acc, { _id, date_posted, name }) => {
      return [...acc, {
        id: _id, createdAt: date_posted, title: name, media: getImage(name)
      }];
    }, []);
  });

export const fetchCategoriesDetail = createAsyncThunk('products/fetchCategories',
  async () => {
    const {
      data
    } = await api.fetchServices();
    return data.reduce((acc, { id, name, count }) => {
      return [...acc, {
        id, title: name, media: getImage(name), count
      }];
    }, []);
  });

const productsSlice = createSlice({
  name: 'products',
  initialState: {
    categories: [],
    services: [],
    error: null,
    status: 'loading'
  },
  reducers: {},
  extraReducers: {
    [fetchCategoriesDetail.fulfilled]: (state, action) => {
      state.status = 'succeeded';
      state.categories = action.payload;
    },
    [fetchCategoriesDetail.rejected]: (state, action) => {
      state.status = 'failed';
      state.error = action.error.message;
    }
  }
});
// export const { setToken } = authSlice.actions;

export default productsSlice.reducer;
